package com.company;

public class Main {
    public static void main(String... args) {
        GreetingFactory
                .instance
                .superMaioGreeting()
                .greet();
    }
}

interface Greeting {
    void greet();
}

class GreetingFactory {
    public static final GreetingFactory instance = new GreetingFactory();

    public Greeting helloWorldGreeting() {
        return new HelloWorldGreeting();
    }

    public Greeting superMaioGreeting() {
        return new SuperMarioGreeting();
    }
}


/////

abstract class AbstractGreeting implements Greeting {
    public abstract String getMessage();

    @Override
    public void greet() {
        System.out.println("This is the greeting");
        System.out.println(getMessage());
    }
}

class HelloWorldGreeting extends AbstractGreeting {
    @Override
    public String getMessage() {
        return "Hello World!";
    }
}

class SuperMarioGreeting extends AbstractGreeting {
    @Override
    public String getMessage() {
        return "Super mario must die!!!";
    }
}

